package io.nerd.triggercicd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@RestController
@RequestMapping("/trigger")
public class TriggerCicdApplication {

    private String token = "glptt-7489d764cf8d6d9d9056f690082b3f76dfbdbf45";
    // https://gitlab.com/api/v4/projects/53122736/ref/main/trigger/pipeline?token=glpat-MoFBr9WTHVVNcg6vM3w7
    private String url = "https://gitlab.com/api/v4/projects/53122736/ref/main/trigger/pipeline?token=" + token;

    public static void main(String[] args) {
        SpringApplication.run(TriggerCicdApplication.class, args);
    }


    @GetMapping("/cicd")
    public String triggerCicd() {
        callGitlab();
        return "Triggered";
    }


    private void callGitlab() {
        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> requestEntity = new HttpEntity<>(headers);

        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<String> responseEntity = restTemplate.exchange(
                url,
                HttpMethod.POST,  // or HttpMethod.GET, HttpMethod.PUT, etc. depending on your API
                requestEntity,
                String.class);

        if (responseEntity.getStatusCode().is2xxSuccessful()) {
            System.out.println("Request successful");
            System.out.println("Response: " + responseEntity.getBody());
        } else {
            System.out.println("Request failed with status code: " + responseEntity.getStatusCodeValue());
        }
    }

}
