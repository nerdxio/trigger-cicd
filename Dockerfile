FROM amazoncorretto:21.0.1-al2023-headful

COPY target/*.jar /app.jar

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "/app.jar"]